<?php
include "conectasql.php";

$temas_filtro = "SELECT * FROM tema ORDER BY NOME";
$res_temas = $conexao ->query($temas_filtro);

$documento_alunos_sem_cadastro = "SELECT IFNULL(rg,cpf) as documento, id_aluno as id FROM `aluno_sem_iipcnet`
WHERE `fl_questionario_de_pesquisa` = 0";
$res_documento_alunos_sem_cadastro = $conexao ->query($documento_alunos_sem_cadastro);

$nome_alunos_sem_cadastro = "SELECT nome, id_aluno as id FROM `aluno_sem_iipcnet`
WHERE `fl_questionario_de_pesquisa` = 0";
$res_nome_alunos_sem_cadastro = $conexao ->query($nome_alunos_sem_cadastro);
?>

<HTML>
<HEAD>
    <TITLE>IIPC BH - Questionário de Pesquisa</TITLE>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="bootstrap-4.0.0-beta.3/dist/css/bootstrap.min.css">
    <script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="jquery-ui.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-4.0.0-beta.3/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    
    <script>
        $(document).ready(function () {  
            $("#nascimento").datepicker({
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                nextText: 'Próximo',
                prevText: 'Anterior'
            });

            $(".td_check").click( function(){
                if ($(this).css("background-color") == "rgb(0, 128, 0)"){
                    //Desselecionou
                    $(this).css("background-color", "rgb(255, 255, 255)");
                    nome = $(this).attr('name');
                    $("#"+nome).val(0);  
                }
                else {
                    //Selecionou
                    $(this).css("background-color", "rgb(0, 128, 0)");
                    nome = $(this).attr('name');
                    $("#"+nome).val(nome); 
                }
            });

            $("#nome_aluno").change(function (){
                $.ajax({
                    url: "busca_documento.php",
                    type: "post",
                    dataType: "json",
                    data: "id_aluno="+$(this).val(),
                    success: function(resultado){
                        $("#rg").val(resultado[0]['documento']);
                        $("#telefone").val(resultado[0]['telefone']);
                    }
                })
            });
            $("#rg").change(function(){
                $.ajax({
                    url: "busca_nome.php",
                    type: "post",
                    dataType: "json",
                    data: "id_aluno="+$(this).val(),
                    success: function(resultado){
                        $("#nome_aluno").val(resultado[0]['aluno']);
                        $("#telefone").val(resultado[0]['telefone']);
                    }
                })
            });
            

        });

        var counter = 0;
            function addInput(divName){
                    counter++;
                    var newdiv = document.createElement('div');
                    newdiv.id = "dynamicInput_child"+counter;
                    newdiv.innerHTML = " <br><input id='select_interesse"+ counter +"' style='background-color: white;' type='text' class='form-control input-lg interesse_extra'> <input type='hidden' id='select_interesse"+ counter +"ID' type='text' class='interesse_extra' name='select_interesse[]'>";
                    document.getElementById(divName).appendChild(newdiv);

                    $( "#select_interesse"+counter+"").autocomplete({
                            source: "listacurso.php",
                            minLength: 2, 
                            open: function(event) {
                                valid = false;
                            },
                            close: function(event){
                                if (!valid) $(this).val('');
                            },
                            select: function (event, ui) {
                                valid = true;
                                $("#"+ this.id +"ID").val(ui.item.id);
                            }
                    });  
            }

            function removeInput(){
                    if (counter > 0) {
                            $( "#dynamicInput_child"+counter ).remove();
                            counter--;
                    }
            }

            function enviar_dados(){
                if ($("form")[0].checkValidity()) {
                    $.ajax({
                        url: "enviar.php",
                        type: "post",
                        data: $("form").serialize(),
                        success: function(){
                            alert("Aluno cadastrado com sucesso!");
                            location.reload();
                        }
                    })
                }else{
                    alert('Preencha todos os campos obrigatórios!\n"Nome", "Cidade", "Estado" e "Gênero"');
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
    </script>

</HEAD>
<body class="bg-light">
    <div class="container">
        <!-- CABEÇALHO -->
        <div class="py-5 text-center">
                <img class="d-block mx-auto mb-4" src="Images/IIPC.png" alt="" width="200">
                <h2>Cadastro de Novo Aluno</h2>
                <p class="lead"></p>
        </div>

        <!-- INFORMAÇOES BÁSICAS DO ALUNO -->
        <form id="frm_info">
            <div class="card">
                <h5 class="card-header">Informações Básicas do Aluno</h5>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-8">
                            <label for="nome_aluno">Nome</label>
                            <select class="custom-select d-block w-100" name="nome_aluno" id="nome_aluno" required="">
                                <option value="">Escolha...</option>
                                <?php 
                                while ($linha_nome = $res_nome_alunos_sem_cadastro -> fetch_assoc()){
                                    ?>
                                    <option value="<?=$linha_nome['id']?>"><?=utf8_encode($linha_nome['nome'])?>
                                    </option>
                                    <?php 
                                }  
                            ?>
                            </select>
                        </div>
                        <div class="col-md-4" style="text-align:center">
                            <label for="genero">Gênero</label>
                            <div class="row" style="margin-left: auto; margin-right: auto; display:block">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="genero" value="masculino" autocomplete="off" required=""> Masculino
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="genero" value="feminino" autocomplete="off" required=""> Feminino
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-5">
                            <label for="rg">RG ou CPF</label>
                            <select class="custom-select d-block w-100" name="rg" id="rg" required="">
                                <option value="">Escolha...</option>
                                <?php 
                                while ($linha_documento = $res_documento_alunos_sem_cadastro -> fetch_assoc()){
                                    ?>
                                    <option value="<?=$linha_documento['id']?>"><?=utf8_encode($linha_documento['documento'])?>
                                    </option>
                                    <?php 
                                }  
                            ?>
                            </select>
                        </div>
                        <div class="col-md-7">
                            <label for="profissao">Profissão</label>
                            <input type="text" style="background-color: white;" class="form-control input-lg" id="profissao" name="profissao">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="cidade">Cidade</label>
                            <input value="Belo Horizonte" required type="text" style="background-color: white;" class="form-control input-lg" id="cidade" name="cidade">
                        </div>
                        <div class="col-md-4">
                            <label for="estado">Estado</label>
                            <input value ="MG" required maxlength="2" type="text" style="background-color: white;" class="form-control input-lg" id="estado" name="estado">
                            <small class="text-muted">Ex: MG, SP</small>
                        </div>
                        <div class="col-md-4">
                            <label for="nascimento">Data de Nascimento</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <input class="form-control input-lg" id="nascimento" type="text" name="nascimento"  value=""/>
                                </div>
                            </div> 
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-5">
                            <label for="telefone">Telefone/Celular</label>
                            <input type="number" style="background-color: white;" class="form-control input-lg" id="telefone" name="telefone">
                        </div>
                        <div class="col-md-7">
                            <label for="email">E-Mail</label>
                            <input type="email" style="background-color: white;" class="form-control input-lg" id="email" name="email">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-5" style="text-align:center">
                            <label for="fl_whatsapp">Aceita receber informações por WhatsApp?</label>
                            <div class="row" style="margin-left: auto; margin-right: auto; display:block">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="fl_whatsapp" value="1" autocomplete="off"> Sim
                                    </label>
                                    <label class="btn btn-secondary">
                                        <input type="radio" name="fl_whatsapp" value="0" autocomplete="off"> Não
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="whatsapp">WhatsApp</label>
                            <input type="number" style="background-color: white;" class="form-control input-lg" id="whatsapp" name="whatsapp">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <label for="obs">Observações</label>
                            <textarea maxlength="500" type="text" class="form-control input-lg" id="obs" name="obs" placeholder="" value="" rows="3" style="overflow:hidden"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <!-- INTERESSES EM CURSOS -->
        <div class="row" style="height:25px;"></div>
        <div class="card">
            <h5 class="card-header">Cursos de Interesse</h5>
            <form id="frm_cursos">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-bordered table-hover" style="font-size: small; text-align:center;">
                                <thead>
                                    <tr>
                                        <td style="font-size: medium; font-weight: bold;">Cursos de Entrada</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td name="51" class="td_check">Assistenciologia</td>
                                        <input id="51" type="hidden" name="select_interesse[]" value ="0">
                                    </tr>
                                    <tr>
                                        <td name="4" class="td_check">CIP</td>
                                        <input id="4" type="hidden" name="select_interesse[]" value ="0">
                                    </tr>
                                    <tr>
                                        <td name="98" class="td_check">CPC</td>
                                        <input id="98" type="hidden" name="select_interesse[]" value ="0">
                                    </tr>
                                    <tr>
                                        <td name="74" class="td_check">Pacifismologia</td>
                                        <input id="74" type="hidden" name="select_interesse[]" value ="0">
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <h6 for="select_interesse1" style="font-size: medium; font-weight: bold;">Outros Cursos</h6>                                               
                            <div id="dynamicInput"></div>
                            <input type="button" class="btn btn-secondary my-2" value="Adicionar curso" onClick="addInput('dynamicInput');">
                            <input type="button" class="btn btn-secondary my-2" value="Remover curso" onClick="removeInput()">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        <!-- TEMAS DE INTERESSE -->
        <div class="row" style="height:25px;"></div>
        <div class="card">
            <h5 class="card-header">Temas de Interesse</h5>
            <div class="card-body col-md-7" style="margin-left: auto; margin-right: auto; display:block">
                <form id="frm_temas">
                    <table class="table table-bordered table-hover" style="font-size: small;">
                        <tbody>
                            <?php 
                                $i = 0;
                                while ($linha_temas = $res_temas -> fetch_assoc()){
                                    $i++;
                                    if ($i % 2 != 0){echo('<tr>');}
                                ?>
                                    <th name ="<?=$linha_temas['id']?>" class="td_check" style="background-color: white"><?=UTF8_ENCODE($linha_temas['NOME'])?></th>
                                    <input id="<?=$linha_temas['id']?>" type='hidden' name="tema_interesse[]" value="0">
                                <?php 
                                    if ($i % 2 == 0) {echo('</tr>');}
                                }  
                            ?> 
                        </tbody>
                    </table>
                </form>
            </div>
        </div> 
        <div class="row">
            <button onClick="enviar_dados();" style="margin-top: 2%" class="btn btn-lg btn-primary btn-block" type="submit">Registrar Dados</button> 
        </div>      
        <div class="row" style="height:100px;"></div>
    </div>
</body>
</HTML>